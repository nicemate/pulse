import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { TaskPage } from '../task/task';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  // Progress bar common params
  max: number = 100;
  stroke: number = 5;
  radius: number = 28;
  rounded: boolean = true;
  responsive: boolean = true;

  // Current values
  wellbeing: number = 68;
  budget: number = 42;
  time_mgt: number = 56;

  constructor(public navCtrl: NavController) {
    
  }

  openNewTaskPage() {
    this.navCtrl.push(TaskPage);
  }

  getOverlayStyle() {
    let transform = ('translateY(-50%) ') + 'translateX(-50%)';

    return {
      'top': '50%',
      'bottom': 'auto',
      'left': '50%',
      'transform': transform,
      '-moz-transform': transform,
      '-webkit-transform': transform,
      'font-size': this.radius / 3.5 + 'px'
    };
  }
}
