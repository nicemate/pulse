import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';

@Component({
  selector: 'page-task',
  templateUrl: 'task.html'
})
export class TaskPage {
  
  constructor(public navCtrl: NavController) {
    
  }

  addTaskPage() {
    // Do something
    this.navCtrl.push(HomePage);
  }

}
