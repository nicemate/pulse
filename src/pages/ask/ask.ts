import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ChatPage } from '../chat/chat';

@Component({
  selector: 'page-ask',
  templateUrl: 'ask.html'
})
export class AskPage {
  
  constructor(public navCtrl: NavController) {
    
  }

  openChat() {
    this.navCtrl.push(ChatPage);
  }

}
