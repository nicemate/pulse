import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-hint',
  templateUrl: 'hint.html'
})
export class HintPage {
  
  constructor(public navCtrl: NavController) {
    
  }

  viewHintPage() {
    // Do something
  }

}
