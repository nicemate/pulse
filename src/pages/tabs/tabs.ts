import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { StatsPage } from '../stats/stats';
import { HintPage } from '../hint/hint';
import { AskPage } from '../ask/ask';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = StatsPage;
  tab3Root = HintPage;
  tab4Root = AskPage;

  constructor() {

  }
}
