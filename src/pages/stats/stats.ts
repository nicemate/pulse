import { Component, ViewChild  } from '@angular/core';

import { Chart } from 'chart.js';

@Component({
  selector: 'page-stats',
  templateUrl: 'stats.html'
})
export class StatsPage {
  @ViewChild('lineChart') lineChart;

  public data : any = {
    "wellbeing" : [
      {
        'time' : '1',
        'rate' : 50
      },
      {
        'time' : '2',
        'rate' : 15
      },
      {
        'time' : '3',
        'rate' : 10
      },
      {
        'time' : '4',
        'rate' : 5
      },
      {
        'time' : '5',
        'rate' : 10
      },
      {
        'time' : '6',
        'rate' : 10
      }
   ],

   "budget" : [
    {
      'time' : '1',
      'rate' : 40
    },
    {
      'time' : '2',
      'rate' : 35
    },
    {
      'time' : '3',
      'rate' : 32
    },
    {
      'time' : '4',
      'rate' : 75
    },
    {
      'time' : '5',
      'rate' : 20
    },
    {
      'time' : '6',
      'rate' : 30
    }
 ],

 "timeMgt" : [
  {
    'time' : '1',
    'rate' : 78
  },
  {
    'time' : '2',
    'rate' : 56
  },
  {
    'time' : '3',
    'rate' : 18
  },
  {
    'time' : '4',
    'rate' : 32
  },
  {
    'time' : '5',
    'rate' : 47
  },
  {
    'time' : '6',
    'rate' : 12
  }
]
};

  public lineChartEl : any;
  public chartLabels : any = [];
  public wellbeingValues : any = [];
  public budgetValues : any = [];
  public timeMgtValues : any = [];
  public chartValues : any = [];

  constructor() {

  }

  ionViewDidLoad() 
  {
    let k : any;    
    for(k in this.data.wellbeing)
    {
      this.chartLabels.push(this.data.wellbeing[k].time);

      this.wellbeingValues.push(this.data.wellbeing[k].rate);
      this.budgetValues.push(this.data.budget[k].rate);
      this.timeMgtValues.push(this.data.timeMgt[k].rate);
    }

    this.chartValues = [
      this.createLine('Wellbeing', this.wellbeingValues, '#488aff'),
      this.createLine('Budget', this.budgetValues, '#32db64'),
      this.createLine('TimeMgt', this.timeMgtValues, '#cf2d63'),
    ];

    this.createLineChart(this.chartLabels, this.chartValues);   
  }

  createLine(label : string, data : any[], color : string, fill : boolean = false){
    return {
      label : label,
      data : data,
      backgroundColor : color,
      borderColor : color,
      fill : fill,
    }
  }

  createLineChart(labels : any[], values : any[]) : void
  {
    this.lineChartEl = new Chart(this.lineChart.nativeElement, 
      {
        data: {
          labels: labels,
          datasets: values
       },
        easing: 'easeInQuart',
        fill: false,
        duration: 2000,
        type: 'line',
        options : {
          maintainAspectRatio: false,
          legend   : {
              display     : true,
              boxWidth    : 80,
              fontSize    : 15,
              padding     : 0
          },
          scales: {
              yAxes: [{
                ticks: {
                    beginAtZero:true,
                    stepSize: 5,
                    max : 100
                }
              }],
              xAxes: [{
                ticks: {
                    autoSkip: false
                }
              }]
          }
        }
      });
  }
}
